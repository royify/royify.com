---
lang: zh-CN
title: Markdown 中使用 Vue
description: Markdown 中使用 Vue,Markdown 中允许使用 HTML,Vue 模板语法是和 HTML 兼容的.
---
<h1>Markdown 中使用 Vue</h1>
<h1>Markdown 中使用 Vue2</h1>
_你好， {{ msg }}_
<RedDiv>

_当前计数为： {{ count }}_

</RedDiv>
<button @click="count++">点我！</button>

<script setup>
import { h, ref } from 'vue'
const RedDiv = (_, ctx) => h(
    'div',
    {
        class: 'red-div',
    },
    ctx.slots.default()
)
const msg = 'markdown 中的 vue'
const count = ref(0)
</script>

<style>
.red-div {
    color: red;
    font-style: normal;
}
</style>