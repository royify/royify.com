---
lang: zh-CN
title: Markdown使用文档 royify.com
description: Markdown是一种简单的文本格式设置方法，在任何设备上都很好看。它不做任何花哨的事情，比如改变字体大小、颜色或类型——只做一些基本的事情，使用你已经知道的键盘符号。royify.com
---
# Markdown
[Markdown 中使用 Vue](./guide/README.md)
<br/>
[Markdown 中使用 Vue2](./guide/markvue.md)
<br/>
### * * 倾斜
*Hello World*           (* *被单星号包括是倾斜)
### ** ** 倾斜加粗
**Hello World**         (** **被双星号包括是倾斜加粗)
# Hello World           (# 一个井号是标题h1) 
## Hello World          (# 两个井号是标题h2)
### Link标签
[Link](http://www.royify.com)
实例    
[Link][1]
[1]: http://www.royify.com 
#### image图片标签 
![Image](https://commonmark.org/help/images/favicon.png)
实例
![Image][1]
⋮
[1]: http://url/b.jpg
### > Hello World tip标签
>Hello World tip标签
### * List 无序列表
实例
* List
* List
* List
* List
### List 有序列表(标签前边添加 1. )
1. One
2. Two
3. Three
### 在文字下边添加下边框使用 --- 或者 ***
Horizontal rule:
---
###  `` 双反引号 添加背景
`Inline code` with backticks
### ``` 连续3个反引号 引用代码块
```
# code block
print '3 backticks or'
print 'indent 4 spaces'
```
### 表格
* [表格](https://docs.github.com/en/get-started/writing-on-github/working-with-advanced-formatting/organizing-information-with-tables) 
* [删除线](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax#styling-text)
### 相对路径 绝对路由
[首页](./index.md)
[首页](index.md)
### Emoji :EMOJICODE:
:EMOJICODE:
:data:
### 目录 [[toc]]
[[toc]]
### 行高亮
```ts{1,6-8}
import { defaultTheme, defineUserConfig } from 'vuepress'

export default defineUserConfig({
  title: '你好， VuePress',

  theme: defaultTheme({
    logo: 'https://vuejs.org/images/logo.png',
  }),
})
```
<!-- end -->
<!-- end -->
<!-- end -->